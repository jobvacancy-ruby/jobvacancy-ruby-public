Candidate Backlog
=================


1. Deactivate offers after 25 days
2. When creating offers, provide the option to have them published on Twitter
3. Fix search making it to be case-insentive
4. Offerer rank
5. Keep record of applications
6. Reactivate offers
7. Support search by location and description