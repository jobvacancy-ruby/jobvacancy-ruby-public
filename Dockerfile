FROM ruby:2.2.0

LABEL maintener nicopaez@computer.org

RUN mkdir -p /var/www/jobvacancy

WORKDIR /var/www/jobvacancy

COPY . /var/www/jobvacancy

RUN git checkout master

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev postgresql-server-dev-9.4 --fix-missing

RUN bundle install --system --without test development

EXPOSE 3000

CMD ["/var/www/jobvacancy/start_app.sh"]
