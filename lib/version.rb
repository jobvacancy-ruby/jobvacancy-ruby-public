class Version

	MAYOR = 1
	MINOR = 2
  BUILD = 0

	def self.current
		"#{MAYOR}.#{MINOR}.#{BUILD}"
	end
end